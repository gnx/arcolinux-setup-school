#!/bin/bash
echo "Изпълняване на първоначална настройка на компютъра"


# Следващите редове са нужни само при пълния ArcoLinux и са излишни при ArcoLinuxB
echo
echo "Изтриване на излишните пакети"
sudo pacman -Rs --noconfirm variety
#sudo pacman -Rs --noconfirm xfce4-clipman-plugin
#sudo pacman -Rs --noconfirm sublime-text-dev
#sudo pacman -Rs --noconfirm qbittorrent
#sudo pacman -Rs --noconfirm arcolinux-meta-steam
#sudo pacman -Rs --noconfirm nomacs
#sudo pacman -Rs --noconfirm playonlinux
#sudo pacman -Rs --noconfirm arcolinux-conky-collection-git
#sudo pacman -Rs --noconfirm conky-lua-archers

echo
echo "Обновяване на системата"
sudo pacman -Syu --noconfirm

echo
echo "Инсталиране на липсващите програми"
sudo pacman -S --noconfirm --needed base-devel gimp inkscape scribus blender libreoffice-fresh jdk8-openjdk jdk-openjdk arduino firefox-ublock-origin audacity kdenlive mousepad mariadb dbeaver lightdm

echo
echo "Инсталиране на пакети от AUR"
yay -Syu --needed --noconfirm eclipse-java lightdm-guest veyon

echo
echo "Задействане на lightdm"
systemctl disable sddm
systemctl enable lightdm

echo
echo "Променяме настройките на гост потребителя"
sudo cp -Rv ./config/.config /etc/skel
sudo rm -R /etc/skel/.config/autostart

echo
echo "Настройване на git"
git config --global user.email "hristo.dr.hristov@gmail.com"
git config --global user.name "Hristo Hristov"

echo "Настройване на клавиатурата"
echo "Настоящите настройки са:"
setxkbmap -print -verbose 10
echo "Задаване на новите настройки"
sudo localectl --no-convert set-x11-keymap us,bg pc105 ,phonetic grp:alt_shift_toggle

# за презаписване на bootloader-а на Windows, мога да въведа в cmd под Windows като Админ:
# bcdedit /set {bootmgr} path \EFI\ArcoLinux\grubx64.efi

# за премахване на трите въпроса за новите потребители
# Отварям gpedit.msc
# Стигам до Computer Configuration > Administrative Templates > Windows Components > Credential User Interface
# Променям Prevent the use of security questions for local accounts на enabled

# за създаване на гост потребител
# отварям cmd
# net user Visitor /add /active:yes
# пита ме за парола и аз натискам 2 пъти Enter, за да го създаде без парола
# премахвам гост потребителя от групата с обикновени потребители
# net localgroup users Visitor /delete
# и го добавям към групата за гости
# net localgroup guests Visitor /add

# за админ
# net user Admin2 /add /active:yes
# net localgroup Administrators Admin2 /add
# net user Admin2 parola
